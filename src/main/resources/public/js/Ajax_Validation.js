$(function() {
	$('#myform').validate({
		errorClass : "my-error-class",
		validClass : "my-valid-class",
		rules : {
			name : {
				minlength : 2,
				lettersonly : true
			},
			pincode : {
				digits : true
			},
			phone : {
				digits : true
			},
			password : {
				minlength : 5
			},
			confirmPassword : {
				minlength : 5,
				equalTo : "#userPassword"
			}
		},
		messages : {
			name : {
				minlength : 'Please enter at least 2 characters.',
				lettersonly : 'Only letters are allowed. Space is not allowed.'
			},
			pincode : {
				digits : 'Only digits are allowed.'
			},
			phone : {
				digits : 'Only digits are allowed.'
			}
		},
		submitHandler : function() {
			var data = $('#myform').serialize();
			$.ajax({
				url : 'rest/users',
				data : data,
				type : 'post',
				dataType : 'json'
			}).then(function(data) {
				return $.ajax({
					url : '/registration.html',
					data : data,
					type : 'post',
					dataType : 'html'
				});
			}).then(function(data){
				$('body').html(data);
			});
		}
	});
});