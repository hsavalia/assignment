package com.k15t.pat.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.k15t.pat.JerseyConfig;
import com.k15t.pat.pojo.User;


public class RegistrationTest extends JerseyTest {
	
	private static final String baseURL = "http://localhost:8080/rest";

	@Override
    protected Application configure() {
        ApplicationContext context = new AnnotationConfigApplicationContext();
        return new JerseyConfig()
                .property("contextConfig", context);
    }

	/**
	 * This test case is used to verify if POST end point for 'users' is working or not.
	 */
	@Test
    public void testPostUser() {
		// create client
		Client client = ClientBuilder.newClient();
		// add form params
		Form form = new Form();
        form.param("name", "Hiren")
        	.param("password", "encoded")
        	.param("email", "hiren_879@yahoo.com")
        	.param("phone","1234")
        	.param("city", "Navsari");
        WebTarget target = client.target(baseURL).path("users");
        Response response = target
				        		.request(MediaType.APPLICATION_JSON)
				        		.post(Entity.form(form));
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Map<String, Object> jsonResponse = response.readEntity(Map.class);
		ObjectMapper mapper = new ObjectMapper();
		User user = (User) mapper.convertValue(jsonResponse, User.class);
		assertEquals(user.getName(), "Hiren");
		assertEquals(user.getEmail(), "hiren_879@yahoo.com");
		assertEquals(user.getPhone(), "1234");
		assertEquals(user.getAddress().getCity(), "Navsari");
        client.close();
    }
	
	/**
	 * This test case is used to verify if user having userID 2 is properly fetched via REST end point or not. 
	 */
	@Test
	public void testGetUser() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(baseURL).path("users").path("2");
		Response response = target
								.request(MediaType.APPLICATION_JSON_TYPE).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		Map<String, Object> jsonResponse = response.readEntity(Map.class);
		ObjectMapper mapper = new ObjectMapper();
		User user = (User) mapper.convertValue(jsonResponse, User.class);
		assertEquals(user.getName(), "Messi");
		assertEquals(user.getEmail(), "Messi@gmail.com");
		assertEquals(user.getPhone(), "9898797988");
		assertEquals(user.getId(), 2);
		assertEquals(user.getAddress().getState(), "ArgState");
		assertEquals(user.getAddress().getCountry(), "Argentina");
		client.close();
	}
	
	/**
	 * This test case checks if all users are correctly returned by REST end point or not.
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@Test
	public void testGetAllUsers() throws JsonParseException, JsonMappingException, IOException {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(baseURL).path("users");
		Response response = target.request(MediaType.APPLICATION_JSON_TYPE).get();
		String jsonResponse = response.readEntity(String.class);
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		ObjectMapper objectMapper = new ObjectMapper();
		List<User> userList = objectMapper.readValue(jsonResponse, new TypeReference<List<User>>(){});
		// If Test Case : testPostUser will run before this test case then total user in system would be 3 else it would be 2.
		// Hence checking for always >2 condition
		assertTrue(userList.size() >= 2);
		client.close();
	}
}
