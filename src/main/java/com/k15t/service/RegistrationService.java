package com.k15t.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.k15t.DAO.UserDao;
import com.k15t.DAO.UserDaoImpl;
import com.k15t.exception.DataNotFoundException;
import com.k15t.exception.MalformedBodyException;
import com.k15t.pat.pojo.Address;
import com.k15t.pat.pojo.User;

/**
 * This class will responsible for processing data and handle business logic.
 * From here one can initial database CRUD operations.
 * 
 * @author hsavalia
 *
 */

@Service
public class RegistrationService {

	@Autowired
	private UserDao userDao;

	public List<User> getAllRegisteredUsers() {
		return userDao.getUsers();
	}

	public User addNewUser(String name, String password, String email, String phone, String street, String city,
			String state, String country, String pinCode) {
		
		// As of now checking only null & empty string but in future we can extend this logic and check for other important validations.
		if (name == null || name.isEmpty()) throw new MalformedBodyException("Name must not be NULL or EMPTY.");
		if (password == null || password.isEmpty()) throw new MalformedBodyException("Password must not be NULL or EMPTY.");
		if (email == null || email.isEmpty()) throw new MalformedBodyException("Email must not be NULL or EMPTY.");
		if (street == null || street.isEmpty()) throw new MalformedBodyException("Street must not be NULL or EMPTY.");
		if (city == null || city.isEmpty()) throw new MalformedBodyException("City must not be NULL or EMPTY.");
		if (state == null || state.isEmpty()) throw new MalformedBodyException("State must not be NULL or EMPTY.");
		if (country == null || country.isEmpty()) throw new MalformedBodyException("Country must not be NULL or EMPTY.");
		if (pinCode == null || pinCode.isEmpty()) throw new MalformedBodyException("Pin Code must not be NULL or EMPTY.");
		
		// Construct User object
		User user = new User(name, password, email, new Address(street, city, state, country, pinCode));
		// checking nullity & empty because it is optional field
		if (phone != null && !phone.isEmpty()) {
			user.setPhone(phone);
		}
		
		userDao.addUser(user);
		return user;
	}

	public User getUser(int userID) {
		User user = userDao.getUser(userID);
		if (user == null) {
			throw new DataNotFoundException("User with ID : " + userID + " not found.");
		}
		return user;
	}

}
