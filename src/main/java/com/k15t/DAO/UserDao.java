package com.k15t.DAO;

import java.util.List;

import com.k15t.pat.pojo.User;

/**
 * This class is created to define all User related DAO methods.
 * @author hsavalia
 *
 */
public interface UserDao {
	public User getUser(int id);
	public List<User> getUsers();
	public void addUser(User user);
}
