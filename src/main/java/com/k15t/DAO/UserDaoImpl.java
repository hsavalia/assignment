package com.k15t.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.k15t.pat.pojo.Address;
import com.k15t.pat.pojo.User;

/**
 * This class is created to implement all DAO methods and to do actual CRUD operations.
 * @author hsavalia
 *
 */
@Repository
public class UserDaoImpl implements UserDao {
	
	/**
	 * Assumption : This is my DB.
	 */
	private static Map<Integer, User> DB = new HashMap<>();
	static
    {
        User user1 = new User("Ronaldo", "encoded", "Ronaldo@gmail.com", new Address("RonaldoStreet", "RonaldoCity", "RonaldoState", "Portugal", "808080"));
        user1.setId(1);
        user1.setPhone("9898797988");
        User user2 = new User("Messi", "encoded", "Messi@gmail.com", new Address("Argstreet", "ArgCity", "ArgState", "Argentina", "909090"));
        user2.setId(2);
        user2.setPhone("9898797988");
        DB.put(user1.getId(), user1);
        DB.put(user2.getId(), user2);
    }

	public static Map<Integer, User> getDB() {
		return DB;
	}
	public static User getUserFromDB(int userId) {
		return DB.get(userId);
	}
	
	@Override
	public User getUser(int id) {
		return DB.get(id);
	}

	@Override
	public List<User> getUsers() {
		return new ArrayList<>(DB.values());
	}

	@Override
	public void addUser(User user) {
		user.setId(DB.values().size()+1);
		DB.put(DB.values().size()+1, user);
	}
}
