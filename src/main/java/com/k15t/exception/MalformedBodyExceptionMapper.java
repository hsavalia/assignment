package com.k15t.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * This exception handler is written to catch malformedbody execption where user do not have provided required body elements.
 * @author hsavalia
 *
 */
public class MalformedBodyExceptionMapper implements ExceptionMapper<MalformedBodyException> {

	@Override
	public Response toResponse(MalformedBodyException exception) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 400, "https://bitbucket.org/hsavalia/assignment");
		return Response
				.status(Status.NOT_ACCEPTABLE)
				.entity(errorMessage)
				.build();
	}

}
