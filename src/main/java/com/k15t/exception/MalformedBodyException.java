package com.k15t.exception;

public class MalformedBodyException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8977303840217095212L;
	
	public MalformedBodyException(String message) {
		super(message);
	}
}
