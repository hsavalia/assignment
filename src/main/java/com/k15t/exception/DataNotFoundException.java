package com.k15t.exception;

public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2433236104223230820L;

	public DataNotFoundException(String message) {
		super(message);
	}
}
