package com.k15t.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * This is exception handler class specific for data not found error.
 * @author hsavalia
 *
 */
@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {

	@Override
	public Response toResponse(DataNotFoundException exception) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 404, "https://bitbucket.org/hsavalia/assignment");
		return Response
				.status(Status.NOT_FOUND)
				.entity(errorMessage)
				.build();
	}

}
