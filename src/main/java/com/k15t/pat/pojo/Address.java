package com.k15t.pat.pojo;

public class Address {
	private String street;
	private String city;
	private String state;
	private String country; 
	private String pincode;
	
	/**
	 * Do not remove Default Constructor.
	 * It has been used by faster Jackson.
	 */
	public Address() {}
	
	public Address(String street, String city, String state, String country, String pinCode) {
		this.street = street;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pincode = pinCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "Address [country=" + country + ", city=" + city + ", street=" + street + ", pincode=" + pincode + "]";
	}
}
