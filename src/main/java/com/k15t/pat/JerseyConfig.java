package com.k15t.pat;

import javax.annotation.PostConstruct;
import javax.ws.rs.Path;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import com.k15t.exception.DataNotFoundExceptionMapper;
import com.k15t.exception.GenericExceptionMapper;
import com.k15t.exception.MalformedBodyException;
import com.k15t.exception.MalformedBodyExceptionMapper;


@Configuration
public class JerseyConfig extends ResourceConfig {

    @Autowired private ApplicationContext applicationContext;

    public JerseyConfig() {
    	register(DataNotFoundExceptionMapper.class);
    	register(MalformedBodyExceptionMapper.class);
    	register(GenericExceptionMapper.class);
	}
    
    @PostConstruct
    public void registerResourcesAndProviders() {
        applicationContext.getBeansWithAnnotation(Path.class).values().forEach(this::register);
    }

}
