package com.k15t.pat.registration;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.k15t.exception.DataNotFoundException;
import com.k15t.pat.pojo.User;
import com.k15t.service.RegistrationService;

@Path("/users")
@Component
public class RegistrationResource {
	
	@Autowired RegistrationService registrationService;
	
	//REST SERVEVICES START
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers() throws URISyntaxException {
        return Response
                .status(200)
                .entity(registrationService.getAllRegisteredUsers())
                .contentLocation(new URI("rest/users/")).build();
    }
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
    public Response createUser(@FormParam("name") String name, @FormParam("password") String password, @FormParam("email") String email, 
    		@FormParam("phone") String phone, @FormParam("street") String street, @FormParam("city") String city,
    		@FormParam("state") String state, @FormParam("country") String country, @FormParam("pincode") String pinCode) throws URISyntaxException {
		User user = registrationService.addNewUser(name, password, email, phone, street, city, state, country, pinCode);
		return Response
	                .status(200)
	                .entity(user)
	                .contentLocation(new URI("rest/users/"+user.getId())).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("id") int userID, @Context HttpServletRequest request)
			throws URISyntaxException {
		User user = registrationService.getUser(userID);
		return Response
				.status(200)
				.entity(user)
				.contentLocation(new URI("rest/users/" + user.getId()))
				.build();
	}
}
