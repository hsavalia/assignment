package com.k15t.pat.registration;

import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.k15t.pat.pojo.User;
import com.k15t.service.RegistrationService;


@RestController
public class RegistrationController {

    @Autowired private VelocityEngine velocityEngine;
    @Autowired private RegistrationService registrationService;
    
    /**
     * This method will handle all request related to registration.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return
     */
    @RequestMapping("/registration.html")
    public String genericRegistrationRequestHandler(HttpServletRequest request, HttpServletResponse response) {
    	User user = null;
    	// check if user id exists in request parameter or not.
    	if (request.getParameter("id") != null) {
    		user = registrationService.getUser(Integer.parseInt(request.getParameter("id")));
		}
    	StringWriter writer = new StringWriter();
    	VelocityContext context = new VelocityContext();
    	Template template;
    	if (user != null) {
    		context.put("user", user);
    		template = velocityEngine.getTemplate("templates/success.vm");
		} else {
			template = velocityEngine.getTemplate("templates/registration.vm");
		}
        template.merge(context, writer);
        return writer.toString();
    }
}
