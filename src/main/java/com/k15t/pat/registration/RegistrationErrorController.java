package com.k15t.pat.registration;

import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationErrorController implements ErrorController {

	@Autowired 
	private VelocityEngine velocityEngine;
	private static final String PATH = "/error";

	@RequestMapping(value = PATH)
	public String error() {
		StringWriter writer = new StringWriter();
		Template template = velocityEngine.getTemplate("templates/error.vm");
		template.merge(new VelocityContext(), writer);
		return writer.toString();
	}
	
	@Override
	public String getErrorPath() {
		return PATH;
	}
}
