# Full-Stack Developer Sample Project

This is a small web application allowing user to register for the next Java Meetup.
This web application includes registration form with fields Name, Phone, Email and Address.
Address field contains 5 more fields namely Street, City, State, Country & Pin Code separately on UI.

In this web application I tried to demonstrate how one can use Controller, Component, Repository & Service concept of Spring framework to design back-end part using mixture of Bootstrap and jquery for UI modeling and validations. REST services are also provided using Jersey Framework. Proper exception messages are shown on any bad request or malformed body.

Proposed Design Solution :

Solution 1 : [Currently Implemented]

1. UI will call REST end point first.
2. REST end points will return success/failure response to UI.
3. UI will make decision accordingly to make Spring framework call.
4. Spring will internally handle this call through controller, process data through services & repositories and return expected view using Velocity.

Solution 2 : 

1. UI will directly call Spring to handle any request.
2. Spring will internally handle this call through controller, process data through services & repositories and return expected view using Velocity.
3. REST ends will be just publicly exposed to make another application on top of it.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

1. Java version above 7 is recommended.
2. Maven enabled Eclipse/Net Beans IDE
3. Latest Chrome/Firefox browser

### Installing

1. Download repository to your local machine.
2. Remove .git folder if you want to push it as your own copy of application.
3. Do Maven clean and install.

###Run the project

You can run project simply by running file **ApplicationBootstrap.java** as a main program from IDE.

###Screen shots

Registration Form Screen

![](images/Registration_Form.PNG)

Required Fields Validation Screen

![](images/Required_Field_Validation.PNG)

Successful Registration Screen

![](images/Successful_Registration.PNG)

##Test manually

Restart server if it is running.

1. Run **ApplicationBootstrap.java** as a main program from IDE.
2. Ping URL : `http://localhost:8080/`
3. Fill the form. Test validations around the forms like enter digits in name field, enter invalid email and check if it working or not. Check if same password functionality is working properly or not.
4. Submit the form. It will show you filled details with success message.
5. Click on "Back" button and register with another user.

##Test REST API

Restart server if it is running.

1. Run **ApplicationBootstrap.java** as a main program from IDE.
2. Ping URL : `http://localhost:8080/`
3. Open REST client i.e. POSTMAN
4. Create a POST request with URL `http://localhost:8080/rest/users`, body **x-www-form-urlencoded** and respective form values. Check **[RESTApi.md](/RESTApi.md)**  for detailed API documentation.
5. Ping POST request and you will get same object with headers and success status 200.
6. As this user is saved in local memory, you can ping GET request to get this object using URL : `http://localhost:8080/rest/users/{userID}`
7. To get all registered user list ping GET on URL : `http://localhost:8080/rest/users`

## Unit Test Cases

Following test cases are included as a part of unit testing.

1. Test POST user
2. Test GET user
3. Test GET all users

#How to perform Unit testing ?

Restart server if it is running. If you face class not found error then please build the project.

1. Run **ApplicationBootstrap.java** as a main program to make REST end points live.
2. Run **RegistrationTest** as a Junit Test to run actual test cases.
3. Check output in JUnit tab in IDE. All test cases should be passed.

## REST API

Following REST APIs are provided.

1. POST: To create a User
2. GET  :  To get information of one User and to get information of all Users.

####How to use REST api ?

**[RESTApi.md](/RESTApi.md)**

##Limitations

1. As of now user can not modify user details once it has been submitted.

##More to do

This is minimalistic version of proposed system hence there is always room to do more and add more functionality in existing application. What are they ? Here is the list of upcoming features/fixes.

1. Add database support to save user information instead of in memory storage.
2. Give user functionality to edit current user details.
3. Add PUT & DELETE REST end points.
4. Show all registered users if such requirement is there.
5. Convert Form parameters directly in User POJO to get efficient code.

### Author
---
[Hiren Savalia](https://github.com/Hiren879)