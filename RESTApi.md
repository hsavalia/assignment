# GET User List

    GET  /users

Returns list of users.


## Parameters
None

## Example
### Request

    GET http://localhost:8080/rest/users

### Response
``` json
[
  {
    "id": 1,
    "name": "Ronaldo",
    "password": "encoded",
    "address": {
      "street": "RonaldoStreet",
      "city": "RonaldoCity",
      "state": "RonaldoState",
      "country": "Portugal",
      "pincode": "808080"
    },
    "email": "Ronaldo@gmail.com",
    "phone": "9898797988"
  },
  {
    "id": 2,
    "name": "Messi",
    "password": "encoded",
    "address": {
      "street": "Argstreet",
      "city": "ArgCity",
      "state": "ArgState",
      "country": "Argentina",
      "pincode": "909090"
    },
    "email": "Messi@gmail.com",
    "phone": "9898797988"
  }
]
```

# GET User

    GET  /user/{userId}

1. Returns a single user.

## Parameters
{userId}

## Example
### Request

    GET http://localhost:8080/rest/users/1
	
### Response
``` json
{
  "id": 1,
  "name": "Ronaldo",
  "password": "encoded",
  "address": {
    "street": "RonaldoStreet",
    "city": "RonaldoCity",
    "state": "RonaldoState",
    "country": "Portugal",
    "pincode": "808080"
  },
  "email": "Ronaldo@gmail.com",
  "phone": "9898797988"
}
```

### Response when user does not exists.
``` json
{
    "errorMessage": "User with ID : 3 not found.",
    "errorCode": 404,
    "documentation": "https://bitbucket.org/hsavalia/assignment"
}
```

# Create User

    POST /users

1. Create a single user and return created user details in JSON format.

## Parameters
None

### JSON Body Parameters
Field | Data Type | Required | Description
--- | --- | --- | ---
name | String | Y | Name of the user
password | String | Y | Password of the user
email | String | Y | Email of the user
phone | String | N | Phone number of the user
street | String | Y | Address [street] of the user.
city | String | Y | Address [city] of the user.
state | String | Y | Address [state] of the user.
country | String | Y | Address [country] of the user.
pincode | String | Y | Address [pincode] of the user.


### How to generate body easily ?

1. Use POSTMAN REST client. 
2. Select POST method.
3. Enter valid POST URL.
4. Go to body tab and fill form {key,value} under option "x-www-form-urlencoded".
5. Hit the send button & check the response.

## Example
### Request

	POST http://localhost:8080/rest/users

### Response
``` json
{
    "id": 6,
    "name": "hiren",
    "password": "somethingEncoded",
    "address": {
        "street": "swagat",
        "city": "gandhinagar",
        "state": "gujarat",
        "country": "india",
        "pincode": "9876"
    },
    "email": "hiren_879@yahoo.com",
    "phone": "1234"
}
```

### Response when any required field is not present in request body.
``` json
{
    "errorMessage": "Email must not be NULL or EMPTY.",
    "errorCode": 400,
    "documentation": "https://bitbucket.org/hsavalia/assignment"
}
```